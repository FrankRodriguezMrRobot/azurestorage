import { Component } from '@angular/core';

import { PopoverController } from '@ionic/angular';

import { PopoverPage } from '../about-popover/about-popover';

import $SP from 'sharepointplus';
import { BlobStorageService } from '../../services/blob-storage.service';
// (window as any).global.$SP = window;
// global.$SP = $SP;
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
  styleUrls: ['./about.scss'],
})
export class AboutPage {
  location = 'madison';
  conferenceDate = '2047-05-17';
  sp;
  selectOptions = {
    header: 'Select a Location'
  };
  sas = "sp=racwdli&st=2021-11-11T23:52:51Z&se=2021-12-31T07:52:51Z&sv=2020-08-04&sr=c&sig=c4j%2BrGs5AUDIKmPaoLjutlX140BIV7lL09qpBVxBwlY%3D";
  accountName="simplexerp";
  containerName="pictures";
  fileToUpload: File | null = null;
  constructor(
    public popoverCtrl: PopoverController,
    public blobStorageService: BlobStorageService
    ) { }

  async presentPopover(event: Event) {
    const popover = await this.popoverCtrl.create({
      component: PopoverPage,
      event
    });
    await popover.present();
  }
  auth(){
    var user1 = {username:'recursoshumanos@limatransvial.com', password:'Transvial2021', domain:'https://limatransvial.sharepoint.com/sites/TestBoletasExterno'};
    // $SP().auth(user1).list("My List","https://limatransvial.sharepoint.com/sites/TestBoletasExterno/").get({});
    this.sp = $SP().auth(user1);
    // sp.list("Test Boletas Externo","https://limatransvial.sharepoint.com/sites/TestBoletasExterno/").get({});
    console.info("sp: ",this.sp);

        // or :
    // var sp = $SP().auth(user1);
    // sp.list("My List", "https://web.si.te").get({...});
    // sp.list("Other List", "http://my.sharpoi.nt/other.directory/").update(...);

    // if you use the AddIn method, it may not work – in that case, you'll need to use the FedAuth method as described there: https://github.com/Aymkdn/SharepointPlus/wiki/Using-the-FedAuth-Cookie#javascript-code
    // let's say we want to use our own function to set the FedAuth cookie
    // var sp = $SP().auth({method:'cookie', function() {
    //   // we need to return the content of our cookie
    //   // e.g. if it's a FedAuth we'll return 'FedAuth=YAHeZNEZdfnZEfzfzeKnfze…';
    //   return myFunctionToGetFedAuth();
    // });
  }
  getList(){
    // this.sp.info().then((resp)=>{
    //   console.info("RESP: ",resp);
    // })
    // this.sp.list("TestBoletasExterno").info().then((infos) => {
    //   console.log(infos);
    //   for (var i=0; i<infos.length; i++) console.log(infos[i]["DisplayName"]+ ": => ",infos[i]);
    //   console.log(infos._List)
    // });
    // console.info(this.sp.getVersion());
    this.sp.getURL().then((resp)=>{
      console.log("URLL SHAREPOINT: ",resp);
    })
    // this.sp.lists().then((lists)=>{
    //   console.log("LISTS: ",lists);
    //   // for (var i=0; i<lists.length; i++) console.log("List #"+i+": "+lists[i].Name);
    // });
  }
  addAttachment(event) {
    // let files = event.target.files;
    // let fileReader = new FileReader();
    // fileReader.onloadend = function(e) {
    //   let content = e.target.result;
    //   $SP().list("MyList").addAttachment({
    //     ID:itemID,
    //     filename:files[0].name,
    //     attachment:content
    //   })
    //   .then(function(url) {
    //     console.log({url})
    //   })
    //   .catch(function(err) {
    //     console.log(err)
    //   })
    // }
    // fileReader.onerror = function(e) {
    //   alert('Unexpected error: '+e.target.error);
    // }
    // fileReader.readAsArrayBuffer(files[0]);
  }
  listImages() {
    this.blobStorageService.listImages(this.accountName, this.containerName, this.sas).then(list => {
      console.info("listImages: ",list);
      // this.picturesList = list
      // const array = []
      // this.picturesDownloaded = array

      // for (let name of this.picturesList) {
      //   this.blobService.downloadImage(this.sas, name, blob => {
      //     var reader = new FileReader();
      //     reader.readAsDataURL(blob);
      //     reader.onloadend = function () {
      //       array.push(reader.result as string)
      //     }
      //   })
      // }
    })
  }

  handleFileInput(files: FileList){
    this.fileToUpload = files.item(0);
    this.blobStorageService.uploadImage(this.accountName, this.containerName, this.sas, this.fileToUpload, new Date().toString(), ()=>{
      console.info("uploadImage");
    })
  }
}
