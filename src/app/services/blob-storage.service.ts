import { Injectable } from '@angular/core';
import { BlobServiceClient , ContainerClient, StorageSharedKeyCredential } from '@azure/storage-blob';

@Injectable({
  providedIn: 'root'
})
export class BlobStorageService {
  constructor() { }

  public uploadImage(accountName, containerName, sas: string, content: Blob, name: string, handler: () => void) {
    this.uploadBlob(content, name, this.containerClient(accountName, containerName, sas), handler)
  }
  public listImages(accountName, containerName, sas){
    return this.listBlobs(this.containerClient(accountName, containerName, sas))
  }
  public downloadImage(accountName, containerName, sas: string, name: string, handler: (blob: Blob) => void) {
    this.downloadBlob(name, this.containerClient(accountName, containerName, sas), handler)
  }
  public deleteImage(accountName, containerName, sas: string, name: string, handler: () => void) {
    this.deleteBlob(name, this.containerClient(accountName, containerName, sas), handler)
  }
  private uploadBlob(content: Blob, name: string, client: ContainerClient, handler: () => void) {
    let blockBlobClient = client.getBlockBlobClient(name);
    blockBlobClient.uploadData(content, { blobHTTPHeaders: { blobContentType: content.type } }).then(() => handler())
  }
  private async listBlobs(client){
    let result;
    let blobs = client.listBlobsFlat();
    for await (const blob of blobs) {
      result.push(blob.name)
    }
    return result;
  }
  private downloadBlob(name: string, client: ContainerClient, handler: (blob: Blob) => void) {
    const blobClient = client.getBlobClient(name);
    blobClient.download().then(resp => {
      resp.blobBody.then(blob => {handler(blob)})
    })
  }
  private deleteBlob(name: string, client: ContainerClient, handler: () => void) {
    client.deleteBlob(name).then(() => {
      handler()
    })
  }
  private containerClient(accountName, containerName, sas){
    return new BlobServiceClient(`https://${accountName}.blob.core.windows.net?${sas}`).getContainerClient(containerName);
  }
}
